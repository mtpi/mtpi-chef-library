require "mtpi/chef/library/version"
require 'gelf'
require 'net/http'

module Mtpi
  module Chef
    module Library

      def prb_send(message, server, port, max_size, facility)
        @notifier = ::GELF::Notifier.new(server, port, max_size, facility => facility)
        @notifier.notify!(:level => ::GELF::Levels::INFO, short_message => message, host => node['hostname'])
      end

      def get_os(node)
        if node['os'] == 'windows'
          return 'Windows'
        elsif node['os'] == 'linux'
          return 'Linux'
        elsif node['os'] == 'aix'
          return 'AIX'
        elsif node['os'] == 'solaris2'
          return 'Solaris'
        else
          return 'PRB-BL-DNS:Data Not Retrieved. Platform is not available.'
        end
      end

      def get_vendor(node)
        if node['os'] == 'windows'
          return 'In developing'
        else
          return `uname -a`.to_s.strip.chomp
        end
      end

      def get_ip(node)
        interfaces = node['network']['interfaces'].keys

        ips = []
        interfaces.each do |interface|
          ip = node['network']['interfaces'][interface]["addresses"].keys.join(',').match(/([0-9]+.[0-9]+.[0-9]+.[0-9]+)/)
          if !ip.nil? || ip[1] == '127.0.0.1'
            ips.push(ip)
          end
        end

        return ips.join(' ')
      end

      def get_os_version(node)
        if node['os'] == 'windows'
          if node['kernel']['os_info']['caption'].to_s.include?('2000')
            return '2000'
          elsif node['kernel']['os_info']['caption'].to_s.include?('2003 R2')
            return '2003 R2'
          elsif node['kernel']['os_info']['caption'].to_s.include?('2003')
            return '2003'
          elsif node['kernel']['os_info']['caption'].to_s.include?('2008 R2')
            return '2008 R2'
          elsif node['kernel']['os_info']['caption'].to_s.include?('2008')
            return '2008'
          elsif node['kernel']['os_info']['caption'].to_s.include?('2012 R2')
            return '2012 R2'
          elsif node['kernel']['os_info']['caption'].to_s.include?('2012')
            return '2012'
          elsif node['kernel']['os_info']['caption'].to_s.include?('2016')
            return '2016'
          else
            return node['kernel']['os_info']['caption']
          end
        elsif node['os'] == 'linux' && node['platform'] == 'redhat'
          return "Red Hat ES #{node['platform_version']}"
        elsif node['os'] == 'aix'
          return "#{node['platform_version']}"
        elsif node['os'] == 'solaris2'
          return "#{node['platform_version'].gsub('5.', '')}"
        else
          return "#{node['platform']} #{node['platform_version']}"
        end
      end

      def download_file(server, port, filePath, downloadFilePath, user, password)
        Net::HTTP.start(server, port) do |http|
          request = Net::HTTP::Get.new filePath
          request.basic_auth user, password

          resp = http.request request
          open(downloadFilePath, "wb") do |file|
            file.write(resp.body)
          end
        end
      end
     end
  end
end
